<?php
foreach ($argv as $i=>$arg )
{
    if ( $arg == "getPath" )
    {
        getPath($argv[$i+1], $argv[$i+2]);
    }
}

/**
 * @param string $relative
 * @param string $base
 * @return string
 */
function getPath(string $relative, string $base = DIRECTORY_SEPARATOR): string
{
    $separator = DIRECTORY_SEPARATOR;
    $regexp = strtr('/^((~\/)|(\.\/))|((\/~)|(\/\.))/', '\/\\', '\\' . $separator);

    if (preg_match($regexp, $base)) {
        throw new InvalidArgumentException('incorrect base path');
    }

    $base = trim($base, $separator);
    $relative = trim($relative, $separator);
    $pathSplit = explode($separator, $relative);

    $pathStack = [];

    foreach ($pathSplit as $element) {
        switch ($element) {
            case '..' :
                array_pop($pathStack);
                break;
            case '.' :
                break;
            //keeps some bash shells
            case '~' :
                $pathStack = [];
                break;
            default:
                array_push($pathStack, $element);
        }
    }

    $result = $separator . $base;

    if (!empty($pathStack)) {
        $relative = implode($separator, $pathStack);
        if ($result == $separator) {
            $result .= $relative;
        } else {
            $result .= $separator . $relative;
        }
    }

    var_dump($result);
    return $result;
}