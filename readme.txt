Usage:
1. run in cli in current folder:
php index.php getPath ./aa/.././bb/ccc/../../aa/some_path/file.txt /home/www/somesite.com/htdocs
where
getPath - current function,
./aa/.././bb/ccc/../../aa/some_path/file.txt - 1st parameter (relative path)
/home/www/somesite.com/htdocs - 2nd (absolute)